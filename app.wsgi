#!/usr/bin/python
import sys
import logging
activate_this = '/var/www/TrainingAppAPI/ta-env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/var/www/TrainingAppAPI/")

from app import app as application