{
	"type": "scenario",
	"level": "Apprentice",
	"brief": "An Employee in an office that you manage the network on is unable to open a program on his Linux workstation. When he attempts to open the program, an error is displayed stating that one or more of the files related to the program are not in the correct director/folder. In this series of questions you will only be given a select number of answers to choose from for each question and consequently only a certain number of attempts may be made. The missing file is in the downloads directory and the program/correct directory is on your desktop. Your job is to locate the missing file and move it to the correct directory using only the terminal on the employee’s computer",
	"base": [
		{
			"name": "pwd",
			"command": "pwd",
			"brief": "",
			"points": 10,
			"successMessage": "Good Choice you now have discovered that you are in the root directory.",
			"scenarioFindings": [

			],
			"stages": [
				{
					"name": "cd Downloads",
					"command": "cd Downloads",
					"points": 10,
					"brief": "",
					"successMessage": "Correct! Now that you are in the directory with the missing file you need to locate it.",
					"scenarioFindings": [

					],
					"stages": [
						{
							"name": "display files",
							"command": "display files",
							"isFail": true,
							"failMessage": "This is not a real command please try again using a proper command."
						},
						{
							"name": "cd ..",
							"command": "cd ..",
							"isFail": true,
							"failMessage": "Wrong! You have gone back to the previous directory without locating the file, but the issue is still unresolved. Please try again."
						},
						{
							"name": "ls",
							"command": "ls",
							"brief": "",
							"points": 10,
							"successMessage": "Good choice, the command ls will show you a list the contents within the current directory. In the list you see the file that is missing.",
							"scenarioFindings": [
							],
							"stages": [
								{
									"name": "mv missingfile.exe /root/Desktop",
									"command": "mv missingfile.exe /root/Desktop",
									"isWin": true,
									"points": 10,
									"scenarioFindings": [
									],
									"winMessage": "Good job! now you have moved the missing file into the correct directory."
								},
								{
									"name": "mv missingFile /root/Desktop",
									"command": "mv missingFile /root/Desktop",
									"isFail": true,
									"failMessage": "mv: missingFile: No such file or directory."
								},
								{
									"name": "move file to Desktop",
									"command": "move file to Desktop",
									"isFail": true,
									"failMessage": "This is not a real command please try again using a proper command."

								},
								{
									"name": "cat missingFile.exe",
									"command": "cat missingFile.exe",
									"isFail": true,
									"failMessage": "This will show you the contents of the file but this does not help you relocate it. Please try again."
								}
							]
						}
					]
				},
				{
					"name": "go to Downloads",
					"command": "go to Downloads",
					"isFail": true,
					"failMessage": "This is not a real command please try again using a proper command."
				},
				{
					"name": "grep",
					"command": "grep",
					"isFail": true,
					"failMessage": "While this could work, you do not know the name of the file that you are looking for. You must manually locate the file. Please try again."
				}
			]
		},
		{
			"name": "ifconfig",
			"command": "ifconfig",
			"isFail": true,
			"failMessage": "Now you have displayed the network configuration. You can see the ip address the subnet mask and various other things. While interesting, this information is not what we need. Please try again."
		},
		{
			"name": "show file",
			"command": "show file",
			"isFail": true,
			"failMessage": "This is not a real command please try again using a proper command."
		}

	]
}