{
	"type": "Common Port Numbers",
	"level": "Journeyman",
	"brief": "In this performance task you will troubleshoot and resolve PC1 from the command line to discover why it cannot communicate with the either of the company�s file servers. You have been tasked with troubleshooting and resolving an issue in your company�s small office. You have physical access to PC1 and PC2 in the command line/IPv4 settings interfaces. The fault in the system as it was explained to you is that PC1 cannot communicate to either of your company�s file servers. However PC2 can communicate to both servers but cannot reach PC1, what would you do? You will only have access to certain commands available (choices are listed below). Note the syntax is very important and incorrect responses will result in repercussions. What commands should you input into the command line to troubleshoot and resolve PC1?",

					//attached is a jpeg of the related network topology created using GNS3 upon start-up of this question the topology should be displayed and also made accessible at any time while on this question.

					"base": [
					         {
					        	 "name": "ipconfig on PC1",
					        	 "command": "ipconfig on PC1",
					        	 "isWin": true,
					        	 "successMessage":"Congrats you successfully entered the correct command, IP config shows the user various information like the IPv4 address, Subnet Mask, Default Gateway This information is very useful when trouble shooting. If something is off in a network, the ability to communicate will not be successful."


		        	 "scenarioFindings": [	
		        	                      "(-----In command line-----)"
		        	                      "c:\users\PC1>ipconfig",
		        	                      "Wireless LAN adapter Wi-Fi:",
		        	                      "IPv4 address: 192.168.0.2",   
		        	                      "Subnet Mask: 255.255.255.0",
		        	                      "Default Gateway: 192.168.1.1",  
		        	                      ],
		        	                      "stages": [

		        	                                 {
		        	                                	 "name": "ipconfig on PC2",
		        	                                	 "command": "ipconfig on PC2",
		        	                                	 "isWin": true,
		        	                                	 "successMessage": "Good Work, you do need to input ipconfig at PC2 to vary the information at PC1. "
		        	                                			 + "Comparing the two finding can help the user find errors in various areas. You make sure that you valid all data before continuing forward.

		        	                                		 	"scenarioFindings": [
		        	                                		 	                     "(-----In command line-----)"
		        	                                		 	                     "c:\users\PC2>ipconfig",
		        	                                		 	                     "Wireless LAN adapter Wi-Fi:",
		        	                                		 	                     "IPv4 address: 192.168.0.3",   
		        	                                		 	                     "Subnet Mask: 255.255.255.0",
		        	                                		 	                     "Default Gateway: 192.168.0.1",
		        	                                		 	                     ],
		        	                                		 	                     "stages": [

		        	                                		 	                                {
		        	                                		 	                                	"name": "netsh interface show interface",
		        	                                		 	                                	"command": "netsh interface show interface",
		        	                                		 	                                	"isWin": true,
		        	                                		 	                                	"winMessage": "Great Job! You have now successfully have entered the correct command. This command allows the users is see all available interfaces on the network."
		        	                                		 	                                			+ "You still have yet discovered why PC1 can�t communicate with the company servers, continue until you discover the next command to enter"
		        	                                		 	                                			"scenarioFindings": [
		        	                                		 	                                			                     "(----------------------In command line-----------------------------)"
		        	                                		 	                                			                     "Admin State      State             Type              Interface Name"
		        	                                		 	                                			                     "--------------------------------------------------------------------"
		        	                                		 	                                			                     "Enabled          Disconnected      Dedicated         Wi-Fi"
		        	                                		 	                                			                     "Enabled          Connected         Dedicated         Ethernet"
		        	                                		 	                                			                     ] 

		        	                                		 	                                			                    		 "stages": [
		        	                                		 	                                			                    		            {
		        	                                		 	                                			                    		            	"name": "go to 4 route change 255.255.255.0 mask 255.255.255.0 192.168.0.1",
		        	                                		 	                                			                    		            	"command": "go to 4 route change 255.255.255.0 mask 255.255.255.0 192.168.0.1",
		        	                                		 	                                			                    		            	"isWin": true,
		        	                                		 	                                			                    		            	"successMessage": "Awesome! You just successfully discovered that the default gateway was incorrect and changed it to the correct one."
		        	                                		 	                                			                    		            			+ "Keep going you are almost there"
		        	                                		 	                                			                    		            			"scenarioFindings": [
		        	                                		 	                                			                    		            			                     "(----------------------In command line-----------------------------)"
		        	                                		 	                                			                    		            			                     "c:\user\PC1>route change 255.255.255.0 mask 255.255.255.0 192.168.0.1"
		        	                                		 	                                			                    		            			                     ]

		        	                                		 	                                			                    		            			                    		 "stages": [
		        	                                		 	                                			                    		            			                    		            {	
		        	                                		 	                                			                    		            			                    		            	"name": "route change 255.255.255.0 mask 255.255.255.0 192.168.0.1",
		        	                                		 	                                			                    		            			                    		            	"command": "route change 255.255.255.0 mask 255.255.255.0 192.168.0.1",
		        	                                		 	                                			                    		            			                    		            	"isWin": true,
		        	                                		 	                                			                    		            			                    		            	"winMessage": "Awesome! You just successfully discovered that the default gateway was incorrect and changed it to the correct one. Congratulations, you are almost there!"
		        	                                		 	                                			                    		            			                    		            		"scenarioFindings": [
		        	                                		 	                                			                    		            			                    		            		                     "(----------------------In command line-----------------------------)"
		        	                                		 	                                			                    		            			                    		            		                     "c:\user\PC1>route change 255.255.255.0 mask 255.255.255.0 192.168.0.1"
		        	                                		 	                                			                    		            			                    		            		                     ]
		        	                                		 	                                			                    		            			                    		            		                    		 "stages": [
		        	                                		 	                                			                    		            			                    		            		                    		            {
		        	                                		 	                                			                    		            			                    		            		                    		            	"name": "192.168.0.4",
		        	                                		 	                                			                    		            			                    		            		                    		            	"command": "192.168.0.4",
		        	                                		 	                                			                    		            			                    		            		                    		            	"isWin": true,
		        	                                		 	                                			                    		            			                    		            		                    		            	"winMessage": "Congratulations, you have successfully discovered and resolved the fault with PC1."
		        	                                		 	                                			                    		            			                    		            		                    		            			+ " By entering in the Ping command, you have verified connectivity between PC1 and the company servers"
		        	                                		 	                                			                    		            			                    		            		                    		            			"scenarioFindings": [
		        	                                		 	                                			                    		            			                    		            		                    		            			                     "(---------------In command line-----------------)"
		        	                                		 	                                			                    		            			                    		            		                    		            			                     "c:\users\PC1> ping 192.168.0.4"
		        	                                		 	                                			                    		            			                    		            		                    		            			                     "Reply from 192.168.0.4: bytes=32 time=62ms TTL=57"
		        	                                		 	                                			                    		            			                    		            		                    		            			                     ]
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		 "stages": [
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            {
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            	"name": "ping 192.168.0.5", 
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            	"command": "ping 192.168.0.5",
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            	"isWin": true,
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            	"winMessage": "Congratulations, you have successfully discovered and resolved the fault with PC1. By entering in the Ping command, "
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            			+ "you have verified connectivity between PC1 and the company servers."
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            			"scenarioFindings": [
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            			                     "(---------------In command line-----------------)"
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            			                     "c:\users\PC1> ping 192.168.0.5"
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            			                     "192.168.0.5: bytes=32 time=61ms TTL=56"
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            			                     ]
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            },
		        	                                		 	                                			                    		            			                    		            		                    		            			                    		            ]
		        	                                		 	                                			                    		            			                    		            		                    		            },
		        	                                		 	                                			                    		            			                    		            		                    		            {
		        	                                		 	                                			                    		            			                    		            		                    		            	"name": "ipconfig",
		        	                                		 	                                			                    		            			                    		            		                    		            	"command": "ipconfig",
		        	                                		 	                                			                    		            			                    		            		                    		            	"isFail": true,
		        	                                		 	                                			                    		            			                    		            		                    		            	"failMessage": "While this is a valid command, you have already completed this step, try again"
		        	                                		 	                                			                    		            			                    		            		                    		            },
		        	                                		 	                                			                    		            			                    		            		                    		            ]



		        	                                		 	                                			                    		            			                    		            },
		        	                                		 	                                			                    		            			                    		            {
		        	                                		 	                                			                    		            			                    		            	"name": "Tracert",
		        	                                		 	                                			                    		            			                    		            	"command": "Tracert",
		        	                                		 	                                			                    		            			                    		            	"isFail": true,
		        	                                		 	                                			                    		            			                    		            	"failMessage": "While this is a valid command, this command is a diagnostic tool to measure the transit delays of a packet. For basic troubleshooting, this would not be the command to use. "
		        	                                		 	                                			                    		            			                    		            			+ "PC1 can�t communicate with the company servers at all, there aren�t necessarily packet loss at the time and the need to measure the delay of packet would not be a valid step"
		        	                                		 	                                			                    		            			                    		            },
		        	                                		 	                                			                    		            			                    		            {
		        	                                		 	                                			                    		            			                    		            	"name": "Ping",
		        	                                		 	                                			                    		            			                    		            	"command": "Ping",
		        	                                		 	                                			                    		            			                    		            	"isFail": true,
		        	                                		 	                                			                    		            			                    		            	"failMessage": "This is a valid command, but there is still communication issues, therefore the ping will fail"
		        	                                		 	                                			                    		            			                    		            },
		        	                                		 	                                			                    		            			                    		            {
		        	                                		 	                                			                    		            			                    		            	"name": "netsh interface ipv4 set route 0.0.0.0/32 ""Local Area Connection"",
		        	                                		 	                                			                    		            			                    		            		"command": "netsh interface ipv4 set route 0.0.0.0/32 ""Local Area Connection"",
		        	                                		 	                                			                    		            			                    		            			"isFail": true,
		        	                                		 	                                			                    		            			                    		            			"failMessage": "This is not a valid response while the command is correct, the syntax is incorrect"
		        	                                		 	                                			                    		            			                    		            },

		        	                                		 	                                			                    		            			                    		            ]		


		        	                                		 	                                			                    		            },
		        	                                		 	                                			                    		            {
		        	                                		 	                                			                    		            	"name": "Tracert 192.168.0.4",
		        	                                		 	                                			                    		            	"command": "Tracert 192.168.0.4",
		        	                                		 	                                			                    		            	"isFail": true,
		        	                                		 	                                			                    		            	"failMessage": "While this is a valid response, this is not an IP address associated with the network. Please try again."
		        	                                		 	                                			                    		            },

		        	                                		 	                                			                    		            ]	 




		        	                                		 	                                }
		        	                                		 	                                {
		        	                                		 	                                	"name": "netsh interface ipv4 set route 0.0.0.0/32 ""Local Area Connection"" 192.168.1.1",
		        	                                		 	                                	"command": "netsh interface ipv4 set route 0.0.0.0/32 ""Local Area Connection"" 192.168.1.1",
		        	                                		 	                                	"isFail": true,
		        	                                		 	                                	"failMessage": "This is not a valid response while the command is correct, the syntax is incorrect"
		        	                                		 	                                },
		        	                                		 	                                ]


		        	                                 }
		        	                                 {
		        	                                	 "name": "Tracert on PC1",
		        	                                	 "command": "Tracert on PC1",
		        	                                	 "isFail": true,
		        	                                	 "failMessage": "While this is a valid command, this command is a diagnostic tool to measure the transit delays of a packet. For basic troubleshooting, this would not be your first step. "
		        	                                			 + "PC1 can�t communicate with the company servers at all, there aren�t necessarily packet loss at the time and the need to measure the delay of packet would not be a valid step"
		        	                                 },

		        	                                 ]

					         },

					         {
					        	 "name": "Ping",
					        	 "command": "Ping",
					        	 "isFail": true,
					        	 "failMessage": " Valid command but not the correct first step towards changing PC1�s default gateway. Try another command"

					         },

					         ]
}
