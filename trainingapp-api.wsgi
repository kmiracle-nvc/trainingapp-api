#!/usr/bin/python
import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/var/www/trainingapp-api")

from trainingapp-api import app as application
application.secret_key = '0ccd512f8c3493797a23557c32db38e7d51ed74f14fa7580'
