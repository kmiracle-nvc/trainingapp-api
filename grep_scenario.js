{
	"type": "scenario",
	"level": "Apprentice",
	"brief": "You are currently working for the HR department for a small business. You are asked to find all company files that have the name Bob in it. More specifically, Bob who just retired, all the files that he has backed up to the company’s Linux server. How would you go about this? In this performance task you will use a Linux terminal to find files that have bob in it. You will only have access to certain commands. Note the syntax is very important and incorrect responses will result in repercussions. What command should you use to find files containing the name bob in it?",
	"base": [
		{
			"name": "grep",
			"command": "grep",
			"points": 10,
			"brief": "",
			"successMessage": "Great job this is the command you will be using to find files with bob in it. Now that you have selected grep as the command to find files, what is right command for finding files with bob in it and ignore word case?",
			"scenarioFindings": [
			],
			"stages": [
				{
					"name": "grep -a bob",
					"command": "grep -a bob",
					"isFail": true,
					"failMessage": "This is not the correct syntax for the task at hand."
				},
				{
					"name": "grep -z bob",
					"command": "grep -z bob",
					"isFail": true,
					"failMessage": "This is not the correct syntax for ignoring word case."
				},
				{
					"name": "grep -i 'bob'",
					"command": "grep -i 'bob'",
					"points": 10,
					"brief": "",
					"successMessage": "Cool, now you are searching for all files and ignoring word case. Now that you have entered the correct command to ignoring word case, the input isn’t specific enough. What command would you use to specify the form of bob as a whole word.",
					"scenarioFindings": [
					],
					"stages": [
						{
							"name": "grep -v",
							"command": "grep -v",
							"isFail": true,
							"failMessage": "This is not the correct command, this command inverts the sense of matching."
						},
						{
							"name": "grep -o",
							"command": "grep -o",
							"isFail": true,
							"failMessage": "Incorrect command for the following task, and is not a valid grep command."
						},
						{
							"name": "grep -w",
							"command": "grep -w",
							"points": 10,
							"brief": "",
							"successMessage": "Awesome! Now you have narrowed down your search even more. Now that you have found the command to find files as bob as a whole word. You want to put all the files in the current directory. What command would you use to complete this task?",
							"scenarioFindings": [
							],
							"stages": [
								{
									"name": "grep -l 'bob' *.c",
									"command": "grep -l 'bob' *.c",
									"isWin": true,
									"points": 10,
									"brief": "",
									"winMessage": "Good work. You now successfully found all files with the name bob on a Linux server. You were able to specify what files you wanted to see and then show the results on the current directory. Congrats! You have completed the task",
									"scenarioFindings": []
								},
								{
									"name": "grep -l 'bob *c",
									"command": "grep -l 'bob *c",
									"isFail": true,
									"failMessage": "This is the incorrect command and you have not successfully completed this task."
								},
								{
									"name": "grep -l 'bob' *.d",
									"command": "grep -l 'bob' *.d",
									"isFail": true,
									"failMessage": "Sorry, but this is not the correct syntax for the following command. You have not successfully completed the task."
								}
							]
						}
					]
				}
			]

		},
		{
			"name": "find",
			"command": "find",
			"isFail": true,
			"failMessage": "While this command could be used, this is not the correct syntax"
		},
		{
			"name": "chmod",
			"command": "chmod",
			"isFail": true,
			"failMessage": "chmod is not the correct command for finding files in a Linux environment."
		}
	]
}