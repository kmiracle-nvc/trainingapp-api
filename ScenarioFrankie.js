{
	"type": "Common Port Numbers",
	"level": "Apprentice",
	"brief": "Marsha, a small business owner, wants to set up her new cisco router she bought. She want to add the router to her preexisting network. Marsha wants to configure the new router (R2) with the IP address of 192.168.0.2 with the same default gateway as Router 1",
	"commandDescription": "In this performance task, you will configure R2 from the console. You will only have access to certain commands available (the choices are listed below). Note the syntax is very important and incorrect responses will result in repercussions.",
	"baseQuestion": "What commands should you put in to configure router 2?"

		//attached is a jpeg of the related network topology created using GNS3 upon start-up of this question the topology should be displayed and also made accessible at any time while on this question.

		"base": [
		         {
		        	 "name": "config t",
		        	 "command": "config t",
		        	 "isWin": true,
		        	 "successMessage": "Great Job, by entering config t you are now in the terminal where global parameters can be set. Now you have to complete the rest of the configuration to enter in the new IP address for R2",
		        	 "scenarioFindings": [
		        	                      " R2 (config) #"
		        	                      ],
		        	                      "stages": [

		        	                                 {
		        	                                	 "name": "int f0/1 ",
		        	                                	 "command": "int f0/1 ",
		        	                                	 "successMessage": "Now you are in the specific interface f0/1 to now change the IP address of R2"
		        	                                		 "scenarioFindings": [
		        	                                		                      "R2 (config-if) #"
		        	                                		                      ],
		        	                                		                      "stages": [

		        	                                		                                 {
		        	                                		                                	 "name": "Open port 3724",
		        	                                		                                	 "command": "Open port 3724",
		        	                                		                                	 "isWin": true,
		        	                                		                                	 "winMessage": "This is the correct command to change the IP address in a specific interface. Now it�s important that all your work isn�t for nothing."
		        	                                		                                		 "scenarioFindings": [
		        	                                		                                		                      "R2 (config-if) # 192.168.0.2 255.255.255.0"
		        	                                		                                		                      ]
		        	                                		                                		                    		  "stages": [

		        	                                		                                		                    		             {
		        	                                		                                		                    		            	 "name": " no shutdown",
		        	                                		                                		                    		            	 "command": " no shutdown",
		        	                                		                                		                    		            	 "successMessage": "Great Job! No shut down changes the administrator from being down to being up, meaning all the changes you made manually will be saved"
		        	                                		                                		                    		            		 "scenarioFindings":	[
		        	                                		                                		                    		            		                    	 "R2 (config-if) # no shutdown"
		        	                                		                                		                    		            		                    	 ]
		        	                                		                                		                    		            		                    			 "stages": [
		        	                                		                                		                    		            		                    			            {
		        	                                		                                		                    		            		                    			            	"name": "exit",
		        	                                		                                		                    		            		                    			            	"command": "exit",
		        	                                		                                		                    		            		                    			            	"successMessage": "Congratulations, you successfully configured R2 with a new IP address!"
		        	                                		                                		                    		            		                    			            		"scenarioFindings": [
		        	                                		                                		                    		            		                    			            		                     "R2 (config-if) # exit"
		        	                                		                                		                    		            		                    			            		                     ]
		        	                                		                                		                    		            		                    			            },

		        	                                		                                		                    		            		                    			            ]
		        	                                		                                		                    		             },
		        	                                		                                		                    		             {
		        	                                		                                		                    		            	 "name": "shutdown",
		        	                                		                                		                    		            	 "command": "shutdown",
		        	                                		                                		                    		            	 "isFail": true,
		        	                                		                                		                    		            	 "failMessage": "Disables all functions specified on the interface, no changes will be made to the IP address"
		        	                                		                                		                    		             },
		        	                                		                                		                    		             {	
		        	                                		                                		                    		            	 "name": "Enter",
		        	                                		                                		                    		            	 "command": "Enter",
		        	                                		                                		                    		            	 "isFail": true,
		        	                                		                                		                    		            	 "failMessage": "By hitting enter you have just created a new line in R2 (config-if). You are not actually doing anything by pressing enter"
		        	                                		                                		                    		             },
		        	                                		                                		                    		             ]


		        	                                		                                 },
		        	                                		                                 {
		        	                                		                                	 "name": "ip route",
		        	                                		                                	 "command": "ip route",
		        	                                		                                	 "isFail": true,
		        	                                		                                	 "failMessage": "This command is used for static routes not for setting the IP address of a router"
		        	                                		                                 },
		        	                                		                                 {
		        	                                		                                	 "name": " ip address 192.168.0.2",
		        	                                		                                	 "command": " ip address 192.168.0.2",
		        	                                		                                	 "isFail": true,
		        	                                		                                	 "failMessage": "This is very close but you forgot to add in the default gateway. In order for you to meet Marsha�s need,"
		        	                                		                                			 + " you need to have the same default gateway as R1 which is 255.255.255.0


		        	                                		                                 }
		        	                                		                                 ]


		        	                                 }
		        	                                 {
		        	                                	 "name": "ip address 10.10.2.2 ",
		        	                                	 "command": "ip address 10.10.2.2 ",
		        	                                	 "isFail": true,
		        	                                	 "failMessage": " Sorry, this is incorrect IP address and you are still in the global parameters, making this entry invalid."
		        	                                 },

		        	                                 {
		        	                                	 "name": "Exit",
		        	                                	 "command": "Exit",
		        	                                	 "isFail": true,
		        	                                	 "failMessage": "Exit is a valid command but R2 is not fully configured yet. Don�t exit yet, keep going."
		        	                                 }
		        	                                 ]
		         },

		         {
		        	 "name": "ip config",
		        	 "command": "ip config",
		        	 "isFail": true,
		        	 "failMessage": "Not a valid command for configuring a new router. "

		         },

		         {
		        	 "name": "ping",
		        	 "command": "ping",
		        	 "isFail": true,
		        	 "failMessage": "Ping is an incorrect response, you want to configure the new router"

		         },
		         ]
}