import os
import json
import random

from app.models import *
from app.database import db_session, init_db


def main():
    init_db()

    users = [{"name": "kyle", "display_name": "Kyle Miracle" },
             {"name": "allen", "display_name": "Allen Raub"}]

    role = "Supervisor"
    password = "password"

    for user in users:
        create_user(user["name"], user["display_name"], password, role)

    # Create a regular user role user
    create_user("alec", "Alec Raub", "password", "User")
    create_user("tyler", "Tyler Hudgins", "password", "User")
    create_user("frankie", "Frankie Raub", "password", "User")

    db_session.commit()

    kyle = User.query.get("1")
    alec = User.query.get("3")
    allen = User.query.get("2")
    tyler = User.query.get("4")
    frankie = User.query.get("5")

    if kyle is not None:
        kyle.direct_reports.append(alec)
        kyle.direct_reports.append(allen)
        kyle.direct_reports.append(tyler)
        kyle.direct_reports.append(frankie)

    allen = User.query.get("2")
    alec = User.query.get("3")
    tyler = User.query.get("4")
    frankie = User.query.get("5")

    if allen is not None:
        allen.direct_reports.append(alec)
        allen.direct_reports.append(tyler)
        allen.direct_reports.append(frankie)

    db_session.add(kyle)
    db_session.add(allen)

    create_scenario()
    db_session.commit()

    # Create ExternalTraining
    external_assignment = ExternalTraining(name="Khan Academy - External Training", level="Apprentice")
    db_session.add(external_assignment)
    db_session.commit()

    # Create Assessment
    assessment1 = Assessment(name="CNE Analyst Assessment", level="Apprentice", num_questions=15)
    assessment2 = Assessment(name="Computer Forensics Assessment", level="Apprentice", num_questions=15)

    db_session.add(assessment1)
    db_session.add(assessment2)
    db_session.commit()

    #scenario1 = Scenario.query.get("1")
    #new_assignment = ScenarioAssignment(track_name="asdfasdf", domain_name="asdfasdf")
    #new_assignment.scenario = scenario1
    #kyle = User.query.get("1")
    #kyle.assigned_scenarios.append(new_assignment)
    #db_session.add(kyle)
    #db_session.commit()

    # Create Domains

    scenario1 = Scenario.query.get("1")
    scenario2 = Scenario.query.get("2")
    external_training = ExternalTraining.query.get("1")
    domain = Domain(domain_name="Computer Basics")
    domain.scenarios.append(scenario1)
    domain.scenarios.append(scenario2)
    domain.external_trainings.append(external_training)
    db_session.add(domain)
    db_session.commit()

    scenario1 = Scenario.query.get("3")
    scenario2 = Scenario.query.get("4")
    domain = Domain(domain_name="Network Exploitation Concepts")
    domain.scenarios.append(scenario1)
    domain.scenarios.append(scenario2)
    db_session.add(domain)
    db_session.commit()

    scenario1 = Scenario.query.get("5")
    domain = Domain(domain_name="Network Routing")
    domain.scenarios.append(scenario1)
    domain.scenarios.append(scenario2)
    db_session.add(domain)
    db_session.commit()

    #scenario1 = Scenario.query.get("1")
    #scenario2 = Scenario.query.get("2")
    #domain = Domain(domain_name="Network Infrastructure")
    #domain.scenarios.append(scenario1)
    #domain.scenarios.append(scenario2)
    #db_session.add(domain)
    #db_session.commit()

    #scenario1 = Scenario.query.get("1")
    #scenario2 = Scenario.query.get("2")
    #domain = Domain(domain_name="Query Syntax and Formatting")
    #domain.scenarios.append(scenario1)
    #domain.scenarios.append(scenario2)
    #db_session.add(domain)
    #db_session.commit()

    #scenario1 = Scenario.query.get("1")
    #scenario2 = Scenario.query.get("2")
    #domain = Domain(domain_name="Mission Understanding")
    #domain.scenarios.append(scenario1)
    #domain.scenarios.append(scenario2)
    #db_session.add(domain)
    #db_session.commit()

    # Create Tracks
    track = Track(track_name="CNE Analyst")
    # Add domain
    domain1 = Domain.query.get("1")
    domain2 = Domain.query.get("2")
    external_training_1 = ExternalTraining.query.get("1")
    assessment = Assessment.query.get("1")
    #domain3 = Domain.query.get("3")
    track.domains.append(domain1)
    track.domains.append(domain2)
    track.external_trainings.append(external_training_1)
    track.assessments.append(assessment)
    track.attachment = "https://www.dropbox.com/s/a5r0i33io2jqssn/CNE_Analyst_Track.docx?dl=1"
    track.attachment_name = "CNE Analyst Training Doc"
    track.track_icon = "black-dragon.gif"
    #track.domains.append(domain3)

    db_session.add(track)
    db_session.commit()

    track = Track(track_name="Computer Forensics")
    # Add domain
    domain1 = Domain.query.get("3")
    external_training_1 = ExternalTraining.query.get("2")
    assessment = Assessment.query.get("2")
    #domain2 = Domain.query.get("5")
    #domain3 = Domain.query.get("6")
    track.domains.append(domain1)
    #track.external_trainings.append(external_training_1)
    track.assessments.append(assessment)
    track.track_icon = "green-dragon.gif"
    #track.domains.append(domain2)
    #track.domains.append(domain3)

    db_session.add(track)
    db_session.commit()

    #track = Track(track_name="Computer Programmer")
    ## Add domain
    #domain1 = Domain.query.get("1")
    #domain2 = Domain.query.get("2")
    #domain3 = Domain.query.get("3")
    #track.domains.append(domain1)
    #track.domains.append(domain2)
    #track.domains.append(domain3)

    #db_session.add(track)
    #db_session.commit()

    #track = Track(track_name="Network Administrator")
    # Add domain
    #domain1 = Domain.query.get("4")
    #domain2 = Domain.query.get("5")
    #domain3 = Domain.query.get("6")
    #track.domains.append(domain1)
    #track.domains.append(domain2)
    #track.domains.append(domain3)

    #db_session.add(track)
    #db_session.commit()

    # Add Kyle to the first track
    #kyle = User.query.get("1")
    #track1 = Track.query.get("1")
    #track2 = Track.query.get("2")
    #kyle.assigned_tracks.append(track1)
    #kyle.assigned_tracks.append(track2)
    #db_session.add(kyle)
    #db_session.commit()

    # Create achievements
    unlockable = Unlockable(item_name="Advanced Theme", type="theme", points_needed=30)
    db_session.add(unlockable)
    db_session.commit()
    unlockable = Unlockable(item_name="Advanced Avatar", type="avatar", points_needed=50)
    db_session.add(unlockable)
    db_session.commit()
    unlockable = Unlockable(item_name="80 Point Achievement", type="theme", points_needed=80)
    db_session.add(unlockable)
    db_session.commit()
    unlockable = Unlockable(item_name="100 Point Achievement", type="theme", points_needed=100)
    db_session.add(unlockable)
    db_session.commit()


def create_user(username, display_name, password, role):
    print "creating user %s - %s" % (username, password)
    user = User(username=username, display_name=display_name, role=role)
    user.hash_password(password)
    db_session.add(user)


def random_level():
    return random.choice(['Apprentice', 'Trainer', 'Journeyman'])
    

def create_scenario():
    json_file = open("scenario1.js")
    scenario1 = Scenario(name="Basic Network Analysis",
                         level="Apprentice",
                         points_possible=30,
                         json_obj=json.load(json_file))
    json_file = open("scenario2.js")
    scenario2 = Scenario(name="Firewall Scenario",
                         level="Apprentice",
                         points_possible=30,
                         json_obj=json.load(json_file))
    json_file = open("scenario-alec.js")
    scenario3 = Scenario(name="Troubleshooting Scenario",
                         level="Journeyman",
                         points_possible=70,
                         json_obj=json.load(json_file))
    json_file = open("linux_cmd_line_scenario.js")
    scenario4 = Scenario(name="Linux Command Line Scenario",
                         level="Apprentice",
                         points_possible=40,
                         json_obj=json.load(json_file))
    json_file = open("grep_scenario.js")
    scenario5 = Scenario(name="Grep Scenario",
                         level="Apprentice",
                         points_possible=40,
                         json_obj=json.load(json_file))

    for scenario in [scenario1, scenario2, scenario3, scenario4, scenario5]:
        db_session.add(scenario)


if __name__ == '__main__':
    main()