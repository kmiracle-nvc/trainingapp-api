{
	"type": "scenario",
	"level": "Apprentice",
	"brief": "You have been asked to perform a basic network analysis on your company's network. The network is not a Domain. Instead, the network is simply managed by the access point. As a deliverable from the task, you are asked to provide a list of all IP addresses, MAC addresses, and identify any users. The computers are not located in a manner to feasibly allow you to physically visit any, however you will have to perform this analysis from your trusty Windows 8 desktop. In the performance of this task, you have only certain commands available (the choices are listed below). Type the selection into the text field below. Note that syntax is very important and incorrect responses will result in severe repercussions. Which command should you run?",
	"base": [
		{
			"name": "net view",
			"command": "net view",
			"points": 10,
			"brief": "Now that you have a list of computers in the network, however in order to further your task, you will need to learn more about each. Which command would you like to try next?",
			"successMessage": "Good choice. The net view command displays a list of shared resources, and when used without any options, it displays a list of computers in the network. Fortunately, many computer names are based on user names.",
			"scenarioFindings": [
                "\\\\Muhammed",
                "\\\\Enrique",
                "\\\\Jiminy",
                "\\\\Marco",
                "\\\\Xaio",
                "\\\\Ubuntu",
                "\\\\WDTVLiveHub"
			],
			"stages": [
				{
					"name": "net view -more",
					"command": "net view -more",
					"isFail": true,
					"failMessage": "Really? What exactly are you trying to do. Go back and try a real command."
				},

				{
					"name": "arp -a",
					"command": "arp -a",
					"points": 10,
					"brief": "You now have a list of IP addresses and MAC addresses for each device on the network, however you will need also need to associate the list of addresses to hostnames. Which command would you like to try next?",
					"successMessage": "Good choice. The arp command displays the IP-to-Physical address translation tables used by the address resolution protocol.",
					"scenarioFindings": [
					    "Internet Address Physical Address Type",
					    "192.168.1.1 c8-d7-19-96-d6-c9 dynamic",
                        "192.168.1.100 b0-83-fe-89-a9-82 dynamic",
                        "192.168.1.102 00-90-a9-a2-00-f2 dynamic",
                        "192.168.1.131 c4-34-6b-0e-bd-64 dynamic",
                        "192.168.1.132 d0-53-49-85-fb-3e dynamic",
                        "192.168.1.137 d0-53-49-34-fa-43 dynamic",
                        "192.168.1.141 d0-53-49-97-bd-64 dynamic",
                        "192.168.1.158 d0-53-49-85-fb-e0 dynamic",
                        "192.168.1.255 ff-ff-ff-ff-ff-ff static"
					],
					"stages": [
						{
							"name": "net view -hostnames",
							"command": "net view -hostnames",
							"isFail": true,
							"failMessage": "Really? What exactly are you trying to do. Go back and try a real command."
						},
						{
							"name": "arp -everything",
							"command": "arp -everything",
							"isFail": true,
							"failMessage": "Really? What exactly are you trying to do. Go back and try a real command."
						},
						{
							"name": "nbtstat -A 192.168.1.131",
							"command": "nbtstat -A 192.168.1.131",
							"points": 10,
							"isWin": true,
							"winMessage": "Good choice. The nbtstat command displays protocol statistics and current TCP/IP connections using NBT (NetBIOS over TCP/IP). By entering nbtstat -A for each IP address in the list, you are able to piece together the following. Congratulations! You have now successfully completed basic network analysis on your company's network. While the 192.168.1.1 & 192.168.1.255 addresses did not associate with a name while using NBT, you are told they are the router and broadcast addresses.",
							"scenarioFindings": [
                                "Internet Address Physical Address Type",
                                "192.168.1.1 c8-d7-19-96-d6-c9 dynamic",
                                "192.168.1.100 b0-83-fe-89-a9-82 dynamic Ubuntu",
                                "192.168.1.101 00-90-a9-a2-00-f4 dynamic WDTVLiveHub",
                                "192.168.1.131 c4-34-6b-0e-bd-64 dynamic Muhammed",
                                "192.168.1.132 d0-53-49-85-fb-3e dynamic Enrique",
                                "192.168.1.137 d0-53-49-34-fa-43 dynamic Jiminy",
                                "192.168.1.141 d0-53-49-97-bd-64 dynamic Marco",
                                "192.168.1.158 d0-53-49-85-fb-e0 dynamic Xaio",
                                "192.168.1.255 ff-ff-ff-ff-ff-ff static"
							]
						}
					]
				},
				{
					"name": "nbtstat -a 192.168.1.255",
					"command": "nbtstat -a 192.168.1.255",
					"isFail": true,
					"failMessage": "Really? What exactly are you trying to do. Go back and try a real command."
				}
			]
		},

		{
			"name": "ping",
			"command": "ping 192.168.1.255",
			"isFail": true,
			"failMessage": "While this appears to be a promising command, you are using a Windows desktop and not a real computer. Try ping –b 192.168.1.255 using Linux for better results. For now, go back and try another command."
		},

		{
			"name": "ipconfig",
			"command": "ipconfig",
			"isFail": true,
			"failMessage": "The IP Config will provide excellent details for the box you are using. However, it will do provide little of the information you need for this scenario. Some noteworthy details that are provided include the interface addresses, default gateway, and DNS suffix. For now, go back and try another command."
		}
	]
}