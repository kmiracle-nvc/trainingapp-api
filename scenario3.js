{
	"type": "Common Port Numbers",
	"level": "Apprentice",
	"brief": "Some of the employees in your office are having difficulties accessing websites that require a secure login, such as their time card, online banking and even some social media sites. As the network administrator you have the ability to alter/change things such as the router configuration from your own windows 8 workstation. In this series of questions you will only be given a select number of answers to choose from for each question and consequently only a certain number of attempts may be made (2? 3?) before penalties are given (Restart the level? Checkpoints?). What should you do to locate the issue. You may choose from the following options:",
    "base": [
             {
                 "name": "192.168.1.1",
                 "command": "192.168.1.1",
                 "successMessage": "Good Choice now you have logged into the admin account and have these available options to choose from –",
                 "scenarioFindings": [
                      "(Go into the Firewall settings under Security settings)",
                      "(Go into Application and Gaming settings)",
                      "(Go into Wireless settings)",
                      "(Exit)"
                  ],
                  "stages": [

                             {
                                 "name": "Firewall settings under Security settings",
                                 "command": "Firewall settings under Security settings",
                                 "successMessage": "Now that you are in the firewall settings you see a list of specific ports that are open and closed on the network. The ports listed as closed are port 3724, 28960 and port 443. You have the option to open or close any of these listed ports. You may also choose to save and exit ",
                                 "scenarioFindings": [
                                     "Open port 3724",
                                     "Open port 28960",
                                     "Open port 443",
                                     "Exit"
                                  ],
                                  "stages": [

                                             {
                                                 "name": "Open port 3724",
                                                 "command": "Open port 3724",
                                                 "isFail": true,
                                                 "failMessage": "Congrats you can now play WoW on your work computer,"
                                                         + " yet still the issue is unresolved. Please try again. "
                                             },
                                             {
                                                 "name": "Open port 28960",
                                                 "command": "Open port 28960",
                                                 "isFail": true,
                                                 "failMessage": "Congrats you can now play Call of Duty on your work computer,"
                                                         + " yet still the issue is unresolved. Please try again. "
                                             },
                                             {
                                                 "name": "Open port 443",
                                                 "command": "Open port 443",
                                                 "isWin": true,
                                                 "winMessage": "Correct, by opening port 443 you allow the protocol HTTPS to operate therefore allowing websites/servers that require a secure connection to communicate.",
                                                 "scenarioFindings": []
                                             }
                                 ]
                             }
                             {
                                 "name": "Application and Gaming settings",
                                 "command": "Application and Gaming settings",
                                 "isFail": true,
                                 "failMessage": "There is nothing here that can help you resolve the issue. Please try again."
                             },

                             {
                                 "name": "Wireless settings",
                                 "command": "Wireless settings",
                                 "isFail": true,
                                 "failMessage": "There is nothing here that can help you resolve the issue. Please try again."
                             }
                     ]
		         },

		         {
		        	 "name": "ipconfig",
		        	 "command": "ipconfig",
		        	 "isFail": true,
		        	 "failMessage": "Now you have displayed the network configuration. You can see the ip address the subnet mask and various other things, "
		        			 + "while interesting this information is not what we need. Please try again (if try = 1 or 2) (else sorry bye)"

		         },

		         {
		        	 "name": "Search for Router Settings",
		        	 "command": "Search for Router Settings",
		        	 "isFail": true,
		        	 "failMessage": "Congratulations you have just pulled up about 27,000,000 Google search results in 0.27 seconds. However that is not the task at hand. Please try again (if try = 1 or 2 - else sorry bye)."

		         },

		         {
		        	 "name": "exit",
		        	 "command": "exit",
		        	 "isFail": true,
		        	 "failMessage": "Are you sure you’d like to exit? Would you like to save you current progress?"
		         }
		         ]
}

