from copy import deepcopy
from flask import abort, request, jsonify, g, Response
from flask.ext import excel
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

from app import app
from app.models import *
from app.database import db_session
from app.custom_basic_auth import CustomHTTPBasicAuth
from app import play


auth = CustomHTTPBasicAuth()


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@auth.verify_password
def verify_password(token):
    """ Verifies the user's token """
    user = User.verify_auth_token(token)
    if not user:
        return False
    g.user = user
    return True


@app.route('/api/token', methods=['POST'])
def get_auth_token():
    """
    Get a token for the user if the user sends the correct credentials
    """
    username = request.json.get('username')
    pwd = request.json.get('password')
    user = User.query.filter_by(username=username).first()
    if not user or not user.verify_password(pwd):
        abort(403)
    else:
        token = user.generate_auth_token()
        return jsonify({'token': token.decode('ascii')})


@app.route('/api/logout', methods=['POST'])
@auth.login_required
def logout():
    return None


""" USERS """


# List of users.
@app.route('/api/user/all', methods=['GET'])
@auth.login_required
def users():
    all_users = User.query.all()
    serialized_users = []
    for user in all_users:
        serialized_user = user.serialize
        serialized_user["directReports"] = []
        serialized_user["supervisors"] = []
        if user.direct_reports:
            for dr in user.direct_reports:
                serialized_user["directReports"].append(dr.serialize)
        if user.supervisors:
            for s in user.supervisors:
                serialized_user["supervisors"].append(s.serialize)
        serialized_users.append(serialized_user)
    return jsonify({"users": serialized_users})


# Get specific user
@app.route('/api/user/get/<user_id>')
@auth.login_required
def user_get(user_id):
    user = User.query.get(user_id)
    if user is None:
        abort(400)

    return jsonify({
        "user": user.serialize
    })


# Add a user.
@app.route('/api/user/add', methods=['POST'])
def add_user():
    password = request.json.get('password')
    role = request.json.get('role')
    email = request.json.get('email')
    first_name = request.json.get('firstName')
    last_name = request.json.get('lastName')
    supervisor_id = request.json.get('supervisorId')
    confirm_password = request.json.get('confirm')
    if password is None or confirm_password is None or email is None:
        abort(400)    # Missing arguments.
    if password != confirm_password:
        abort(400)  # Passwords do not match.
    if User.query.filter_by(username=email).first() is not None:
        abort(400)  # Existing user.

    user = User(username=email, role=role, email_address=email, display_name=first_name+" "+last_name)
    user.hash_password(password)
    db_session.add(user)
    db_session.commit()

    if supervisor_id is not None:
        supervisor = User.query.get(supervisor_id)
        if supervisor is not None:
            supervisor.direct_reports.append(user)
            db_session.add(supervisor)
            db_session.commit()

    return jsonify({'user': user.serialize}), 201


# Get user profile info
@app.route('/api/user/info', methods=['GET'])
@auth.login_required
def get_user_info():
    if g.user:
        return jsonify({'user': g.user.serialize})


@app.route('/api/user/tracks/<user_id>', methods=['GET'])
@auth.login_required
def get_user_tracks(user_id=None):
    # Return the user's assigned tracks
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    track_assignments = user.track_assignments

    return jsonify({
        "tracks": [a.serialize for a in track_assignments]
    })


@app.route('/api/user/scenario/<user_id>', methods=['GET'])
@auth.login_required
def get_user_scenarios(user_id=None):
    # Return the user's assigned scenarios
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    scenario_assignments = user.scenario_assignments
    ser_scenarios = []

    for assignment in scenario_assignments:
        ser = assignment.scenario.serialize
        ser["domainName"] = assignment.domain_name
        ser["trackName"] = assignment.track_name
        ser["completed"] = assignment.completed
        ser_scenarios.append(ser)

    return jsonify({
        "scenarios": ser_scenarios
    })


@app.route('/api/user/external-training/<user_id>', methods=['GET'])
@auth.login_required
def get_user_external_training(user_id=None):
    # Return the user's assigned external training
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    ext_training_assignments = user.external_training_assignments
    ser_trainings = []

    for assignment in ext_training_assignments:
        ser = assignment.external_training.serialize
        ser["id"] = assignment.id
        ser["domainName"] = assignment.domain_name
        ser["trackName"] = assignment.track_name
        ser["status"] = assignment.completed
        ser_trainings.append(ser)

    return jsonify({
        "trainings": ser_trainings
    })


@app.route('/api/user/assessment/<user_id>', methods=['GET'])
@auth.login_required
def get_user_assessments(user_id=None):
    # Return the user's assigned assessments
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    assessment_assignments = user.assessment_assignments
    ser_assessments = []

    for assignment in assessment_assignments:
        ser = assignment.assessment.serialize
        ser["id"] = assignment.id
        ser["domainName"] = assignment.domain_name
        ser["trackName"] = assignment.track_name
        ser["status"] = assignment.completed
        ser["grade"] = assignment.serialize["grade"]
        ser["correct"] = assignment.serialize["correct"]
        ser_assessments.append(ser)

    return jsonify({
        "assessments": ser_assessments
    })


@app.route('/api/user/tracks/available/<user_id>', methods=['GET'])
@auth.login_required
def get_user_avail_tracks(user_id=None):
    # Return the user's assigned tracks
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)
    ret_tracks = []
    assigned_tracks = user.assigned_tracks
    assigned_domains = user.assigned_domains
    assigned_external_trainings = user.assigned_external_trainings
    assigned_assessments = user.assigned_assessments
    all_tracks = Track.query.all()
    for track in all_tracks:
        ser_track = track.serialize
        ser_track["assigned"] = False
        for a_track in assigned_tracks:
            if a_track == track:
                ser_track["assigned"] = True

        for ext_tng in ser_track.get("externalTrainings"):
            ext_tng["assigned"] = False
            for a_ext_tng in assigned_external_trainings:
                if ext_tng.get("id") == a_ext_tng.id:
                    ext_tng["assigned"] = True

        for assessment in ser_track.get("assessments"):
            assessment["assigned"] = False
            for a_assessment in assigned_assessments:
                if assessment.get("id") == a_assessment.id:
                    assessment["assigned"] = True

        for domain in ser_track.get("domains"):
            domain["assigned"] = False
            for a_domain in assigned_domains:
                if domain.get("id") == a_domain.id:
                    domain["assigned"] = True

        ret_tracks.append(ser_track)

    return jsonify({
        "tracks": ret_tracks
    })


@app.route('/api/user/assign/<user_id>', methods=['POST'])
@auth.login_required
def user_assign(user_id=None):
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    tracks = request.data.get("tracks")
    domains = request.data.get("domains")
    external_trainings = request.data.get("trainings")
    assessments = request.data.get("assessments")

    # Loop through each track sent in API call and update user's info if necessary
    for track in tracks:
        if track.get("assigned"):
            t = Track.query.get(track.get("id"))
            user.assign_track(t)
        else:
            t = Track.query.get(track.get("id"))
            user.remove_track(t)

    # Assign stand alone domains not part of a track
    for domain in domains:
        if domain.get("assigned"):
            d = Domain.query.get(domain.get("id"))
            user.assign_domain(d, create_assignments=True)
        else:
            d = Domain.query.get(domain.get("id"))
            user.remove_domain(d, remove_assignments=True)

    # Assign stand alone external trainings
    for ext_tng in external_trainings:
        if ext_tng.get("assigned"):
            et = ExternalTraining.query.get(ext_tng.get("id"))
            user.assign_external_training(et)
        else:
            et = ExternalTraining.query.get(ext_tng.get("id"))
            user.remove_external_training(et)

    # Assign stand alone assessments
    for assessment in assessments:
        if assessment.get("assigned"):
            a = Assessment.query.get(assessment.get("id"))
            user.assign_assessment(a)
        else:
            a = Assessment.query.get(assessment.get("id"))
            user.remove_assessment(a)

    db_session.add(user)
    db_session.commit()

    return jsonify({
        "success": True
    })


@app.route('/api/user/report/<user_id>', methods=['GET'])
@auth.login_required
def download_user_status_report(user_id=None):
    # Return the user's assigned tracks
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)
    scenario_assignments = user.scenario_assignments
    external_training_assignments = user.external_training_assignments
    assessment_assignments = user.assessment_assignments

    scenario_assignment_rows = [assignment.csv_row for assignment in scenario_assignments]
    external_training_assignment_rows = [assignment.csv_row for assignment in external_training_assignments]
    assessment_assignment_rows = [assignment.csv_row for assignment in assessment_assignments]
    assignment_rows = scenario_assignment_rows + external_training_assignment_rows + assessment_assignment_rows

    assignment_rows = [["Training Type",
                       "ID",
                       "Track",
                       "Domain",
                       "Training Name",
                       "Status",
                       "Completed Date"]] + assignment_rows

    return excel.make_response_from_array(assignment_rows, "csv")


@app.route('/api/user/report/team/<user_id>', methods=['GET'])
@auth.login_required
def download_user_team_status_report(user_id=None):
    # Return the user's assigned tracks
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    direct_reports = user.direct_reports
    direct_reports.append(user)

    scenario_assignment_rows = []
    external_training_assignment_rows = []
    assessment_assignment_rows = []

    for dr in direct_reports:
        scenario_assignments = dr.scenario_assignments
        external_training_assignments = dr.external_training_assignments
        assessment_assignments = dr.assessment_assignments

        scenario_assignment_rows += [[dr.display_name] + assignment.csv_row for assignment in scenario_assignments]
        external_training_assignment_rows += [[dr.display_name] + assignment.csv_row for assignment in external_training_assignments]
        assessment_assignment_rows += [[dr.display_name] + assignment.csv_row for assignment in assessment_assignments]

    assignment_rows = scenario_assignment_rows + external_training_assignment_rows + assessment_assignment_rows

    assignment_rows = [["Name",
                       "Training Type",
                       "ID",
                       "Track",
                       "Domain",
                       "Training Name",
                       "Status",
                       "Completed Date"]] + assignment_rows

    return excel.make_response_from_array(assignment_rows, "csv")


@app.route('/api/user/status/<user_id>', methods=['GET'])
@auth.login_required
def get_user_training_status(user_id=None):
    user = User.query.get(user_id)
    if user is None:
        abort(400)
    track_assignments = user.track_assignments
    domain_assignments = user.domain_assignments
    scenario_assignments = user.scenario_assignments
    external_assignments = user.external_training_assignments
    assessment_assignments = user.assessment_assignments
    all_assignments = track_assignments + domain_assignments + scenario_assignments

    return jsonify({
        "assignments": [a.serialize for a in scenario_assignments + external_assignments + assessment_assignments]
    })


@app.route('/api/user/status/team/<user_id>', methods=['GET'])
@auth.login_required
def get_user_team_training_status(user_id=None):
    user = User.query.get(user_id)
    if user is None:
        abort(400)

    direct_reports = user.direct_reports
    direct_reports.append(user)
    status_reports = []

    for dr in direct_reports:
        new_report = dict()
        new_report["user"] = dr.serialize
        assignments = []
        assignments += dr.scenario_assignments
        assignments += dr.external_training_assignments
        assignments += dr.assessment_assignments
        new_report["assignments"] = [a.serialize for a in assignments]
        status_reports.append(new_report)

    return jsonify({
        "assignments": status_reports
    })


@app.route('/api/user/external-training/completed/<user_id>', methods=['POST'])
@auth.login_required
def user_mark_completed(user_id=None):
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    training_id = request.data.get("trainingId")
    if training_id is None:
        abort(400)

    training = ExternalTrainingAssignment.query.get(training_id)
    training.completed = True
    db_session.add(training)
    db_session.commit()

    return jsonify({
        "success": True
    })


@app.route('/api/user/achievement/all/<user_id>', methods=['GET'])
@auth.login_required
def get_user_achievements_all(user_id=None):
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    achievements = user.achievements.all()

    return jsonify({
        "achievements": [a.serialize for a in achievements]
    })


@app.route('/api/user/achievement/applied/<user_id>', methods=['GET'])
@auth.login_required
def get_user_applied_achievements(user_id=None):
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)
    if user is None:
        abort(400)

    achievements = [a for a in user.achievements.all() if a.applied]

    return jsonify({
        "achievements": [a.serialize for a in achievements]
    })


""" GAMES """


@app.route('/api/game/all', methods=['GET'])
@auth.login_required
def games():
    users_games = g.user.games.all()
    dr_games = []
    direct_reports = g.user.direct_reports
    for dr in direct_reports:
        dr_games += dr.games.all()

    if users_games is not None:
        ret = [game.serialize for game in users_games+dr_games]
        return jsonify({"games": ret})


@app.route('/api/game/start', methods=['POST'])
@auth.login_required
def start_new_game():
    scenario_id = request.data["scenario"]["id"]
    scenario = Scenario.query.filter_by(id=scenario_id).first()
    if scenario is not None:
        # Determine is the user has any other games started with the same scenario
        prior_games = Game.query.filter_by(scenario_id=scenario_id, user_id=g.user.id).all()
        if len(prior_games) < 1:
            first_attempt = True
        else:
            first_attempt = False
        new_game = Game(g.user, scenario, "base", 0, first_attempt=first_attempt)
        db_session.add(new_game)
        db_session.commit()
        return jsonify({'game': new_game.serialize}), 201


@app.route('/api/game/delete', methods=['POST'])
@auth.login_required
def delete_game():
    """
    Post a Game object to delete it
    """
    game_id = request.data["id"]
    game = Game.query.get(game_id)

    if game is not None:
        db_session.delete(game)
        db_session.commit()
        return jsonify({"message": "Successfully deleted game"})


@app.route('/api/game/next', methods=['POST'])
@auth.login_required
def game_next():
    """
    Pass in a game and return the next question.
    :return:
    """
    game_id = request.data["id"]
    game = Game.query.get(game_id)

    if game is None:
        abort(400)

    return jsonify({
        "brief": play.next_question(game),
        "availableCommands": play.available_commands(game)
    })


@app.route('/api/game/answer', methods=['POST'])
@auth.login_required
def answer_game():
    """
    Pass in a game and answer submission
    :return:
    """
    game_id = request.data["game"]["id"]
    answer = request.data["answer"]
    game = Game.query.get(game_id)

    if game is None:
        abort(400)

    result = play.submit_answer(game, answer)
    correct = result.get("correct")
    message = result.get("message")
    findings = result.get("findings")
    next_stage = result.get("next_stage")
    is_win = result.get("is_win")
    points = int(result.get("points"))

    # If this is the first time attempting stage, set stage_attempted to true
    if game.current_stage_attempted is False:
        first_attempt = True
        game.current_stage_attempted = True
    else:
        # Don't give out any points if the stage has already been attempted
        first_attempt = False
        points = 0

    if correct:
        # If the answer was correct, set current_stage_attempted back to false
        game.current_stage_attempted = False

    game.last_stage = next_stage
    # If the answer was correct, update the last stage of the game
    # If the game is not a first attempt, only allow points that equal total points possible of scenario
    if not game.first_attempt:
        # Find the scores of the previous attempts, find highest score, only allow points to equal points possible
        prior_attempts = Game.query.filter(Game.scenario_id == game.scenario_id,
                                           Game.user_id == g.user.id,
                                           Game.id != game.id).all()
        high_score = sum([prior_game.score for prior_game in prior_attempts])
        new_score = high_score + points
        if new_score > game.scenario.points_possible:
            pass
        elif new_score == game.scenario.points_possible:
            game.score = points
        else:
            game.score = game.score + points

    else:
        game.score = game.score + points
    # If there is a win, change the game status
    if is_win:
        game.status = "Completed"
        game.completed = True
        game.completed_at = func.current_timestamp()

        # mark all scenario assignments that match game as complete
        for assignment in game.user.scenario_assignments:
            if assignment.scenario == game.scenario:
                assignment.completed = True
                assignment.completed_at = func.current_timestamp()
                assignment.score = game.score
                db_session.add(assignment)
                db_session.commit()

    command_obj = {
        "findings": findings
    }

    command = Command(user=g.user, game=game, command_obj=command_obj, command_type="response")
    db_session.add(game)
    db_session.add(command)
    db_session.commit()

    return jsonify({
        "correct": correct,
        "firstAttempt": first_attempt,
        "message": message,
        "scenarioFindings": findings,
        "isWin": is_win,
        "points": points,
        "game": game.serialize
    })


# Trainee Notes
@app.route('/api/game/note/add', methods=['POST'])
@auth.login_required
def add_game_note():
    game_id = request.data["gameId"]
    notes = request.data["notes"]
    game = Game.query.get(game_id)
    if game is None:
        abort(400)
    game.notes = notes
    db_session.add(game)
    db_session.commit()

    return jsonify({
        "game": game.serialize
    })


@app.route('/api/game/command/add', methods=['POST'])
@auth.login_required
def add_game_command():
    game_id = request.data["gameId"]
    command = request.data["command"]
    game = Game.query.get(game_id)
    if game is None:
        abort(400)

    command_obj = {
        "command": command
    }

    new_command = Command(user=g.user, game=game, command_obj=command_obj, command_type="submission")
    db_session.add(new_command)
    db_session.commit()

    return jsonify({
        "success": True
    })


@app.route('/api/game/response/add', methods=['POST'])
@auth.login_required
def add_game_response():
    game_id = request.data["gameId"]
    command = request.data["value"]
    game = Game.query.get(game_id)
    if game is None:
        abort(400)

    command_obj = {
        "findings": [command]
    }

    new_command = Command(user=g.user, game=game, command_obj=command_obj, command_type="response")
    db_session.add(new_command)
    db_session.commit()

    return jsonify({
        "success": True
    })


@app.route('/api/game/command/all/<game_id>', methods=['GET'])
@auth.login_required
def get_game_commands(game_id):
    game = Game.query.get(game_id)

    if game is None:
        abort(400)

    serialized_commands = [command.serialize for command in game.commands]

    return jsonify({
        "commands": serialized_commands
    })


""" Scenarios """


def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['js', 'json'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/api/scenario/add', methods=['POST'])
@auth.login_required
def add_scenario():
    scenario_name = request.data.get("scenarioName")
    level = request.data.get("difficultyLevel")

    scenario_file = request.files['file']
    if scenario_file and allowed_file(scenario_file.filename):
        scenario = Scenario(name=scenario_name, level=level, json_obj=json.load(scenario_file))
        db_session.add(scenario)
        db_session.commit()
        return jsonify({"scenario": scenario}), 201
    else:
        abort(400)


@app.route('/api/scenario/build', methods=['POST'])
@auth.login_required
def post_build_scenario():
    scenario_data = request.data.get("scenario")
    if scenario_data is None:
        abort(400)
    scenario_level = scenario_data.get("level")
    scenario_name = scenario_data.get("name")
    points_possible = scenario_data.get("pointsPossible")

    scenario_json = json.dumps(scenario_data)
    new_scenario = Scenario(name=scenario_name,
                            level=scenario_level,
                            points_possible=points_possible,
                            json_obj=scenario_data)

    db_session.add(new_scenario)
    db_session.commit()
    return jsonify({
        "success": True
    })


@app.route('/api/scenario/all', methods=['GET'])
@auth.login_required
def get_scenario_all():
    scenarios = Scenario.query.all()
    serialized = [scenario.serialize for scenario in scenarios]

    return jsonify({"scenarios": serialized})


@app.route('/api/external/all', methods=['GET'])
@auth.login_required
def get_external_all():
    external_trainings = ExternalTraining.query.all()
    serialized = [e.serialize for e in external_trainings]

    return jsonify({"training": serialized})


@app.route('/api/assessment/all', methods=['GET'])
@auth.login_required
def get_assessment_all():
    assessments = Assessment.query.all()
    serialized = [a.serialize for a in assessments]

    return jsonify({"assessments": serialized})


@app.route('/api/scores')
@auth.login_required
def scores():
    return {'scores': 'scores'}


""" Comments """


@app.route('/api/comment/add', methods=['POST'])
@auth.login_required
def add_comment():
    game_id = request.data["gameId"]
    comment_text = request.data["commentText"]
    game = Game.query.get(game_id)
    if game is None:
        abort(400)

    new_comment = Comment(user=g.user, game=game, comment_text=comment_text)
    db_session.add(new_comment)
    db_session.commit()
    return jsonify({"success": True})


@app.route('/api/comment/all/<game_id>', methods=['GET'])
@auth.login_required
def get_comment_all(game_id):
    game = Game.query.get(game_id)
    if game is None:
        abort(400)
    comments = game.comments.all()
    serialized_comments = [comment.serialize for comment in comments]
    return jsonify({"comments": serialized_comments})


""" Stats """


@app.route('/api/stats/get/<user_id>', methods=['GET'])
@auth.login_required
def get_stats(user_id=None):
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)

    if user is None:
        abort(400)

    serialized_user = user.serialize
    serialized_user["directReports"] = []
    serialized_user["supervisors"] = []
    if user.direct_reports:
        for dr in user.direct_reports:
            serialized_user["directReports"].append(dr.serialize)
    if user.supervisors:
        for s in user.supervisors:
            serialized_user["supervisors"].append(s.serialize)

    return jsonify({
        "stats": user.get_stats(),
        "user": serialized_user
    })


""" Achievements """


@app.route('/api/achievement/check', methods=['GET'])
@auth.login_required
def achievement_check():
    user = g.user
    eligible_unlockables = []

    # Check for unlockables that user is eligible for
    unlockables = Unlockable.query.all()
    for u in unlockables:
        if user.get_stats()["totalPoints"] >= u.points_needed:
            eligible_unlockables.append(u)
    # Filter achievements user already has
    achievements = user.achievements.all()

    def already_unlocked(unlockable):
        ret = False
        for achievement in achievements:
            if achievement.unlockable_id == unlockable.id:
                return True
        return ret

    new_achievements = [Achievement(unlockable=u, user=user) for u in eligible_unlockables if not already_unlocked(u)]
    for achievement in new_achievements:
        db_session.add(achievement)
    db_session.commit()

    return jsonify({
        "new": len(new_achievements) > 0,
        "newAchievements": [unlockable.serialize for unlockable in new_achievements]
    })


@app.route('/api/achievement/update/<achievement_id>', methods=['POST'])
@auth.login_required
def update_achievement(achievement_id=None):
    achievement = Achievement.query.get(achievement_id)
    if achievement is None:
        abort(400)
    applied = request.data.get("applied")
    achievement.applied = applied

    db_session.add(achievement)
    db_session.commit()

    return jsonify({
        "success": True
    })


""" Track """


@app.route('/api/track/all', methods=['GET'])
@auth.login_required
def get_track_all():
    user = g.user
    all_tracks = Track.query.all()

    return jsonify({
        "tracks": [track.serialize for track in all_tracks]
    })


@app.route('/api/track/add', methods=['POST'])
def add_track():
    track_name = request.json.get('trackName')
    domains = request.json.get('domains')
    bonus_points = request.json.get("bonusPoints")

    if track_name is None or domains is None or bonus_points is None:
        abort(400)    # Missing arguments.

    track = Track(track_name=track_name, bonus_points=int(bonus_points))
    for d in domains:
        domain = Domain.query.get(d)
        track.domains.append(domain)
    db_session.add(track)
    db_session.commit()

    return jsonify({'track': track.serialize}), 201


@app.route('/api/domain/add', methods=['POST'])
def add_domain():
    domain_name = request.json.get('domainName')
    scenarios = request.json.get('scenarios')
    bonus_points = request.json.get("bonusPoints")

    if domain_name is None or scenarios is None or bonus_points is None:
        abort(400)    # Missing arguments.

    domain = Domain(domain_name=domain_name, bonus_points=int(bonus_points))
    for s in scenarios:
        scenario = Scenario.query.get(s)
        domain.scenarios.append(scenario)
    db_session.add(domain)
    db_session.commit()

    return jsonify({'track': domain.serialize}), 201


""" Notes (sent from supervisor to trainee) """
@app.route('/api/note/send', methods=['POST'])
@auth.login_required
def send_note():
    note_text = request.data.get("noteText")
    to_id = request.data.get("toId")
    from_id = request.data.get("fromId")

    to_user = User.query.get(to_id)
    from_user = User.query.get(from_id)

    if to_user is None or from_user is None:
        abort(400)

    new_note = Note(to_user=to_user, from_user=from_user, note_text=note_text)
    db_session.add(new_note)
    db_session.commit()

    return jsonify({
        "success": True
    })


@app.route('/api/note/all/<user_id>', methods=['GET'])
@auth.login_required
def get_note_all(user_id):
    if user_id is None:
        user = g.user
    else:
        user = User.query.get(user_id)

    if user is None:
        abort(400)

    all_notes = user.notes_received.all()

    return jsonify({
        "notes": [note.serialize for note in all_notes]
    })


@app.route('/api/note/get/<note_id>', methods=['GET'])
@auth.login_required
def get_note(note_id):
    if note_id is None:
        abort(400)

    note = Note.query.get(note_id)
    if note is None:
        abort(400)
    note.read_flag = True

    db_session.add(note)
    db_session.commit()

    return jsonify({
        "note": note.serialize
    })


@app.route('/api/domain/all', methods=['GET'])
@auth.login_required
def get_domain_all():
    all_domains = Domain.query.all()

    return jsonify({
        "domains": [d.serialize for d in all_domains]
    })


@app.route('/api/assessment/grade', methods=['POST'])
@auth.login_required
def post_grade_assessment():
    assessment_assignment_id = request.data.get("assignmentId")
    answers = request.data.get("answers")

    assignment = AssessmentAssignment.query.get(assessment_assignment_id)
    if assignment is None:
        abort(400)
    assignment.completed = True
    assignment.completed_at = func.current_timestamp()

    results = play.grade_assessment(answers)
    assignment.correct = results["correct"]

    db_session.add(assignment)
    db_session.commit()

    return jsonify({
        "results": results
    })