from flask import g, abort
import functools


def is_admin(f):
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        if g.user.role != 'admin':
            abort(403)
        # invoke the wrapped function
        return f(*args, **kwargs)


def is_trainer(f):
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        if g.user.role != 'trainer':
            abort(403)
        # invoke the wrapped function
        return f(*args, **kwargs)