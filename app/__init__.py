import os

from flask.ext.api import FlaskAPI
from flask_cors import CORS

app = FlaskAPI(__name__)

#cfg_path = os.environ["TRAININGAPP_CONFIG"]
cfg_path = "app.config.TestingConfig"
app.config.from_object(cfg_path)

if app.config["NAME"] == "development":
    cors = CORS(app)

from app.database import init_db

init_db()

from app import routes