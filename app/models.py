from sqlalchemy import *
from sqlalchemy import event
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context
from datetime import datetime

import json
import sqlalchemy as sqla
from sqlalchemy.ext import mutable
from sqlalchemy.orm import relationship, backref

from app import app
from app.database import Base
from app.database import db_session


class JsonEncodedDict(sqla.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = sqla.String

    def process_bind_param(self, value, dialect):
        return json.dumps(value)

    def process_result_value(self, value, dialect):
        return json.loads(value)

mutable.MutableDict.associate_with(JsonEncodedDict)


class AbstractBase(Base):
    __abstract__ = True
    created_at = Column(DateTime, default=func.current_timestamp())
    modified_at = Column(DateTime, default=func.current_timestamp(), onupdate=func.current_timestamp())

""" Scenario Assignment"""

# Supervisor to Direct Reports Association Table
supervisors_reports = Table('supervisors_reports', Base.metadata,
                            Column('supervisor_id', Integer, ForeignKey('users.id', name='a'), primary_key=True),
                            Column('report_id', Integer, ForeignKey('users.id', name='b'), primary_key=True))
# User to Assigned Tracks Association Table
users_tracks_table = Table('users_tracks', Base.metadata,
                           Column('user_id', Integer, ForeignKey('users.id')),
                           Column('track_id', Integer, ForeignKey('tracks.id')))
# User to Assigned Scenarios Association Table
users_domains_table = Table('users_domains', Base.metadata,
                            Column('user_id', Integer, ForeignKey('users.id')),
                            Column('domain_id', Integer, ForeignKey('domains.id')))
# User to Assigned Scenarios Association Table
users_scenarios_table = Table('users_scenarios', Base.metadata,
                              Column('user_id', Integer, ForeignKey('users.id')),
                              Column('scenario_id', Integer, ForeignKey('scenarios.id')))
# User to Assigned External Training Association Table
users_external_trainings_table = Table('users_external_trainings', Base.metadata,
                              Column('user_id', Integer, ForeignKey('users.id')),
                              Column('external_training_id', Integer, ForeignKey('external_trainings.id')))
# User to Assigned Assessment Association Table
users_assessments_table = Table('users_assessments', Base.metadata,
                              Column('user_id', Integer, ForeignKey('users.id')),
                              Column('assessment_id', Integer, ForeignKey('assessments.id')))

""" TrackAssignment """
# User to Assigned Track
ta_users_tracks_table = Table('ta_users_tracks', Base.metadata, Column('user_id', Integer, ForeignKey('users.id')),
                              Column('track_id', Integer, ForeignKey('tracks.id')))
""" DomainAssignment """
# User to Assigned Domain
da_users_domains_table = Table('da_users_domains', Base.metadata, Column('user_id', Integer, ForeignKey('users.id')),
                               Column('domain_id', Integer, ForeignKey('domains.id')))


class Assignment(AbstractBase):
    __abstract__ = True
    completed = Column(Boolean, default=False)
    completed_at = Column(DateTime)

    @property
    def csv_row(self):
        if self.completed:
            status = "Complete"
        else:
            status = "Incomplete"
        if hasattr(self, "scenario") and self.scenario:
            training_name = self.scenario.name
        else:
            training_name = ""
        if hasattr(self, "domain") and self.domain:
            domain_name = self.domain.domain_name
        else:
            domain_name = ""
        if hasattr(self, "track") and self.track:
            track_name = self.track.track_name
        else:
            track_name = ""

        if type(self).__name__ == "ExternalTrainingAssignment":
            assignment_type = "External Training"
            training_name = self.external_training.name
        elif type(self).__name__ == "AssessmentAssignment":
            assignment_type = "Assessment"
            training_name = self.assessment.name
        elif type(self).__name__ == "ScenarioAssignment":
            assignment_type = "Scenario"

        return [
            assignment_type,
            self.id,
            track_name,
            domain_name,
            training_name,
            status,
            self.completed_at
        ]


class ScenarioAssignment(Assignment):
    __tablename__ = 'scenarios_assignments'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    scenario_id = Column(Integer, ForeignKey('scenarios.id'))
    track_id = Column(Integer, ForeignKey('tracks.id'))
    domain_id = Column(Integer, ForeignKey('domains.id'))
    track_name = Column(String(256))
    domain_name = Column(String(256))
    scenario = relationship("Scenario")
    track = relationship("Track")
    domain = relationship("Domain")
    score = Column(Integer)

    @property
    def serialize(self):
        if self.track:
            ser_track = self.track.serialize
        else:
            ser_track = ""
        return {
            'id': self.id,
            'scenario': self.scenario.serialize,
            'domain': self.domain.serialize,
            'track': ser_track,
            'completed': self.completed,
            'completed_at': self.completed_at,
            'score': self.score
        }


class ExternalTraining(AbstractBase):
    __tablename__ = 'external_trainings'
    id = Column(Integer, primary_key=True)
    name = Column(String(256), index=True)
    level = Column(String(32), index=True)
    points_possible = Column(Integer)
    #son_obj = Column(JsonEncodedDict(10000))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'level': self.level,
            'pointsPossible': self.points_possible
        }

    def __repr__(self):
        return self.name + " : " + self.level


class ExternalTrainingAssignment(Assignment):
    __tablename__ = 'external_training_assignments'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    external_training_id = Column(Integer, ForeignKey('external_trainings.id'))
    track_id = Column(Integer, ForeignKey('tracks.id'))
    domain_id = Column(Integer, ForeignKey('domains.id'))
    type = Column(String(256))
    track_name = Column(String(256))
    domain_name = Column(String(256))
    external_training = relationship("ExternalTraining")
    track = relationship("Track")
    domain = relationship("Domain")

    @property
    def serialize(self):
        if self.track:
            ser_track = self.track.serialize
        else:
            ser_track = ""
        if self.domain:
            ser_domain = self.domain.serialize
        else:
            ser_domain = ""
        return {
            'id': self.id,
            'externalTraining': self.external_training.serialize,
            'domain': ser_domain,
            'track': ser_track,
            'completed': self.completed,
            'completed_at': self.completed_at
        }


class TrackAssignment(Assignment):
    __tablename__ = 'tracks_assignments'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User")
    track_id = Column(Integer, ForeignKey('tracks.id'))
    track = relationship("Track")

    @property
    def track_status(self):
        completed = 0
        # Look at each scenario in the track and check if user has a completed scenario that matches
        for scenario in self.track.scenarios:
            # Check for completed ScenarioAssignments
            for assignment in self.user.scenario_assignments:
                if assignment.scenario == scenario and assignment.completed:
                    completed += 1
        for tng in self.track.external_trainings:
            # Check for completed ScenarioAssignments
            for assignment in self.user.external_training_assignments:
                if assignment.external_training == tng and assignment.completed:
                    completed += 1
        for assessment in self.track.assessments:
            # Check for completed ScenarioAssignments
            for assignment in self.user.assessment_assignments:
                if assignment.assessment == assessment and assignment.completed:
                    completed += 1

        items_to_complete = len(self.track.scenarios+self.track.external_trainings+self.track.assessments)
        return {
            "completed": completed,
            "total": items_to_complete
        }

    @property
    def serialize(self):
        return {
            'id': self.id,
            'track': self.track.serialize,
            'trackName': self.track.track_name,
            'bonusPoints': self.track.bonus_points,
            'status': self.track_status,
            'completed': self.completed,
            'domains': [d.serialize for d in self.track.domains]
        }


class DomainAssignment(Assignment):
    __tablename__ = 'domains_assignments'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    domain_id = Column(Integer, ForeignKey('domains.id'))
    domain = relationship("Domain")


class User(AbstractBase):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(32), index=True)
    display_name = Column(String(128))
    role = Column(String(32), index=True)
    email_address = Column(String(128), index=True)
    password_hash = Column(String(256))
    rank = Column(String(128), index=True, default="Apprentice")
    current_track_id = Column(Integer, ForeignKey('tracks.id'))
    current_track = relationship('Track', backref=backref('current_track', lazy='dynamic'))

    assigned_tracks = relationship('Track', secondary=users_tracks_table, backref=backref('users', lazy='dynamic'))
    assigned_domains = relationship('Domain', secondary=users_domains_table, backref=backref('users', lazy='dynamic'))
    assigned_external_trainings = relationship('ExternalTraining', secondary=users_external_trainings_table,
                                              backref=backref('users', lazy='dynamic'))
    assigned_assessments = relationship('Assessment', secondary=users_assessments_table,
                                        backref=backref('users', lazy='dynamic'))
    track_assignments = relationship("TrackAssignment")
    domain_assignments = relationship("DomainAssignment")
    scenario_assignments = relationship("ScenarioAssignment")
    external_training_assignments = relationship("ExternalTrainingAssignment")
    assessment_assignments = relationship("AssessmentAssignment")

    direct_reports = relationship('User',
                                  secondary=supervisors_reports,
                                  primaryjoin=id == supervisors_reports.c.supervisor_id,
                                  secondaryjoin=id == supervisors_reports.c.report_id,
                                  backref='supervisors')

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=10000):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    def assign_track(self, track):
        already_assigned = False
        for assigned_track in self.assigned_tracks:
            if assigned_track == track:
                already_assigned = True
        if not already_assigned:
            self.assigned_tracks.append(track)
            # Create a new TrackAssignment
            new_track_assignment = TrackAssignment(track=track, user=self)
            self.track_assignments.append(new_track_assignment)
            for domain in track.domains:
                self.assign_domain(domain)
                for scenario in domain.scenarios:
                    new_scenario_assignment = ScenarioAssignment(track_name=track.track_name,
                                                                 domain_name=domain.domain_name, track=track,
                                                                 domain=domain)
                    new_scenario_assignment.scenario = scenario
                    self.scenario_assignments.append(new_scenario_assignment)

            for training in track.external_trainings:
                self.assign_external_training(training, track=track)
            for assessment in track.assessments:
                self.assign_assessment(assessment, track=track)

    def remove_track(self, track_to_remove):
        if track_to_remove in self.assigned_tracks:
            self.assigned_tracks.remove(track_to_remove)
            for domain in track_to_remove.domains:
                self.remove_domain(domain)

            for assignment in list(self.scenario_assignments):
                if assignment.track == track_to_remove:
                    self.scenario_assignments.remove(assignment)

            for training in track_to_remove.external_trainings:
                self.remove_external_training(training)

            for assessment in track_to_remove.assessments:
                self.remove_assessment(assessment)

        for assignment in self.track_assignments:
            if assignment.track == track_to_remove:
                self.track_assignments.remove(assignment)

    def assign_domain(self, domain, create_assignments=False):
        already_assigned = False
        for assigned_domain in self.assigned_domains:
            if assigned_domain == domain:
                already_assigned = True
        if not already_assigned:
            self.assigned_domains.append(domain)
            new_domain_assignment = DomainAssignment(domain=domain)
            self.domain_assignments.append(new_domain_assignment)
            if create_assignments:
                for scenario in domain.scenarios:
                    self.scenario_assignments.append(ScenarioAssignment(scenario=scenario, track_name="No Track",
                                                                        track=None, domain=domain,
                                                                        domain_name=domain.domain_name))

    def remove_domain(self, domain_to_remove, remove_assignments=False):
        if domain_to_remove in self.assigned_domains:
            self.assigned_domains.remove(domain_to_remove)
            if remove_assignments:
                # Remove any ScenarioAssignments that match the domain and have no Track associated
                flagged_to_remove = []
                for assignment in self.scenario_assignments:
                    if assignment.track is None and assignment.domain == domain_to_remove:
                        flagged_to_remove.append(assignment)
                for f in flagged_to_remove:
                    self.scenario_assignments.remove(f)
        for assignment in self.domain_assignments:
            if assignment.domain == domain_to_remove:
                self.domain_assignments.remove(assignment)

    def assign_external_training(self, training, track=None):
        already_assigned = False
        for assigned_training in self.assigned_external_trainings:
            if assigned_training == training:
                already_assigned = True
        if not already_assigned:
            self.assigned_external_trainings.append(training)
            new_ext_tng_assignment = ExternalTrainingAssignment(external_training=training, track=track)
            self.external_training_assignments.append(new_ext_tng_assignment)

    def remove_external_training(self, training_to_remove):
        if training_to_remove in self.assigned_external_trainings:
            self.assigned_external_trainings.remove(training_to_remove)
        for assignment in self.external_training_assignments:
            if assignment.external_training == training_to_remove:
                self.external_training_assignments.remove(assignment)

    def assign_assessment(self, assessment, track=None):
        already_assigned = False
        for assigned_assessment in self.assigned_assessments:
            if assigned_assessment == assessment:
                already_assigned = True
        if not already_assigned:
            self.assigned_assessments.append(assessment)
            new_assessment_assignment = AssessmentAssignment(assessment=assessment, track=track)
            self.assessment_assignments.append(new_assessment_assignment)

    def remove_assessment(self, assessment_to_remove):
        if assessment_to_remove in self.assigned_assessments:
            self.assigned_assessments.remove(assessment_to_remove)
        for assignment in self.assessment_assignments:
            if assignment.assessment == assessment_to_remove:
                self.assessment_assignments.remove(assignment)

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

    @property
    def serialize(self):
        if self.current_track is None:
            current_track = "Unassigned"
        else:
            current_track = self.current_track.track_name

        return {
            'id': self.id,
            'username': self.username,
            'displayName': self.display_name,
            'email': self.email_address,
            'role': self.role,
            'rank': self.rank,
            'currentTrack': current_track
        }

    def get_stats(self):
        total_points = 0
        for game in self.games:
            total_points += game.score

        completed_scenarios = [assignment for assignment in self.scenario_assignments if assignment.completed]
        completed_tracks = [assignment for assignment in self.track_assignments if assignment.completed]
        completed_scenarios.sort(key=lambda x: x.completed_at, reverse=True)
        if len(completed_scenarios) > 0:
            last_scenario = completed_scenarios[0].scenario.name
        else:
            last_scenario = ""

        return {
            "user_id": self.id,
            "userDisplayName": self.display_name,
            "totalPoints": total_points,
            "lastScenario": last_scenario,
            "scenariosCompleted": len(completed_scenarios),
            "tracksCompleted": len(completed_tracks)
        }

    def __repr__(self):
        return self.username + " : " + self.role


class Scenario(AbstractBase):
    __tablename__ = 'scenarios'
    id = Column(Integer, primary_key=True)
    name = Column(String(256), index=True)
    level = Column(String(32), index=True)
    points_possible = Column(Integer)
    json_obj = Column(JsonEncodedDict(10000))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'level': self.level,
            'pointsPossible': self.points_possible,
            'json_obj': self.json_obj
        }

    def __repr__(self):
        return self.name + " : " + self.level


class Game(AbstractBase):
    __tablename__ = 'games'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', backref=backref('games', lazy='dynamic'))
    scenario_id = Column(Integer, ForeignKey('scenarios.id'))
    scenario = relationship('Scenario', backref=backref('games', lazy='dynamic'))
    last_stage = Column(String(256))
    current_stage_attempted = Column(Boolean, default=False)
    score = Column(Integer)
    completed = Column(Boolean, default=False)
    completed_at = Column(DateTime)
    status = Column(String(32))
    notes = Column(String(10000))
    first_attempt = Column(Boolean, default=True)

    def __init__(self, user, scenario, last_stage, score, notes="", status="In Progress", first_attempt=True):
        self.user = user
        self.scenario = scenario
        self.last_stage = last_stage
        self.score = score
        self.notes = notes
        self.status = status
        self.first_attempt = first_attempt

    @property
    def serialize(self):
        return {
            'id': self.id,
            'created': self.created_at,
            'completedAt': self.completed_at,
            'isComplete': self.completed,
            'userId': self.user_id,
            'userDisplayName': self.user.display_name,
            'scenarioName': self.scenario.name,
            'scenarioLevel': self.scenario.level,
            'scenario': self.scenario.serialize,
            'lastStage': self.last_stage,
            'currentStageAttempted': self.current_stage_attempted,
            'score': self.score,
            'pointsPossible': self.scenario.points_possible,
            'notes': self.notes,
            'status': self.status,
            'firstAttempt': self.first_attempt
        }


class Command(AbstractBase):
    __tablename__ = 'commands'
    id = Column(Integer, primary_key=True)
    command_obj = Column(JsonEncodedDict(10000))
    command_type = Column(String(256))  # Submission or Response
    game_id = Column(Integer, ForeignKey('games.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    created_time_real = Column(DateTime, default=datetime.utcnow)

    game = relationship('Game', backref=backref('commands', lazy='dynamic'))
    user = relationship('User', backref=backref('commands', lazy='dynamic'))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'createdTime': self.created_time_real.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],
            'userId': self.user_id,
            'gameId': self.game_id,
            'command': self.command_obj,
            'commandType': self.command_type
        }

    def __repr__(self):
        return "<Command " + self.id + " >"


class Comment(AbstractBase):
    __tablename__ = 'comments'
    id = Column(Integer, primary_key=True)
    comment_text = Column(String(10000))
    user_id = Column(Integer, ForeignKey('users.id'))
    game_id = Column(Integer, ForeignKey('games.id'))
    game = relationship('Game', backref=backref('comments', lazy='dynamic'))
    user = relationship('User', backref=backref('comments', lazy='dynamic'))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'created': self.created_at,
            'commentText': self.comment_text,
            'userId': self.user_id,
            'userDisplayName': self.user.display_name,
            'gameId': self.game_id
        }


class Unlockable(AbstractBase):
    """ An Unlockable is an achievement that can be unlocked.
    """
    __tablename__ = 'unlockables'
    id = Column(Integer, primary_key=True)
    item_name = Column(String(256))
    type = Column(String(256))
    points_needed = Column(Integer)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'itemName': self.item_name,
            'pointsNeeded': self.points_needed
        }


class Achievement(AbstractBase):
    """ An Achievement is an instance of an Unlockable that a user has achieved.
    """
    __tablename__ = 'achievements'
    id = Column(Integer, primary_key=True)
    applied = Column(Boolean, default=False)
    user_id = Column(Integer, ForeignKey('users.id'))
    unlockable_id = Column(Integer, ForeignKey('unlockables.id'))
    unlockable = relationship('Unlockable', backref=backref('achievements', lazy='dynamic'))
    user = relationship('User', backref=backref('achievements', lazy='dynamic'))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'applied': self.applied,
            'type': self.unlockable.type,
            'userId': self.user_id,
            'created': self.created_at,
            'unlockableId': self.unlockable_id,
            'achievementName': self.unlockable.item_name,
            'pointsNeeded': self.unlockable.points_needed,
        }


class Note(AbstractBase):
    """ Notes are used by supervisors to send messages to users.
    """
    __tablename__ = 'notes'
    id = Column(Integer, primary_key=True)
    #direct_report_id = Column(Integer, ForeignKey('users.id'))
    #supervisor_id = Column(Integer, ForeignKey('users.id'))
    #direct_report = relationship('User', backref=backref('notes_direct_report', lazy='dynamic'),
                                 #foreign_keys=direct_report_id)
    #supervisor = relationship('User', backref=backref('notes_supervisor', lazy='dynamic'), foreign_keys=supervisor_id)

    to_id = Column(Integer, ForeignKey('users.id'))
    from_id = Column(Integer, ForeignKey('users.id'))
    to_user = relationship('User', backref=backref('notes_received', lazy='dynamic'), foreign_keys=to_id)
    from_user = relationship('User', backref=backref('notes_sent', lazy='dynamic'), foreign_keys=from_id)
    read_flag = Column(Boolean, default=False)
    note_text = Column(String(10000))

    @property
    def serialize(self):
        return {
            'id': self.id,
            'toId': self.to_id,
            'fromId': self.from_id,
            'read': self.read_flag,
            'fromDisplayName': self.from_user.display_name,
            'noteText': self.note_text,
            'created': self.created_at
        }

# Association table for the Track to Scenario many to many relationship
track_domain_table = Table('track_domain', Base.metadata,
                             Column('track_id', Integer, ForeignKey('tracks.id')),
                             Column('domain_id', Integer, ForeignKey('domains.id')))

# Association table for the Track to External Training many to many relationship
track_external_training_table = Table('track_external_training', Base.metadata,
                             Column('track_id', Integer, ForeignKey('tracks.id')),
                             Column('external_training_id', Integer, ForeignKey('external_trainings.id')))

# Association table for the Track to Assessment many to many relationship
track_assessment_table = Table('track_assessment', Base.metadata,
                             Column('track_id', Integer, ForeignKey('tracks.id')),
                             Column('assessment_id', Integer, ForeignKey('assessments.id')))


class Track(AbstractBase):
    """ Tracks consist of many scenarios. A user can be assigned a track which
    will grant them access to multiple scenarios.
    """
    __tablename__ = 'tracks'
    id = Column(Integer, primary_key=True)
    track_name = Column(String(256))
    bonus_points = Column(Integer, default=0)
    domains = relationship("Domain", secondary=track_domain_table, backref="tracks")
    external_trainings = relationship("ExternalTraining", secondary=track_external_training_table, backref="tracks")
    assessments = relationship("Assessment", secondary=track_assessment_table, backref="tracks")
    track_icon = Column(String(256))
    attachment = Column(String(256))
    attachment_name = Column(String(256))

    @property
    def scenarios(self):
        scenarios = []
        for domain in self.domains:
            for scenario in domain.scenarios:
                scenarios.append(scenario)
        return scenarios

    @property
    def serialize(self):
        return {
            'id': self.id,
            'trackName': self.track_name,
            'bonusPoints': self.bonus_points,
            'domains': [d.serialize for d in self.domains],
            'externalTrainings': [e.serialize for e in self.external_trainings],
            'assessments': [a.serialize for a in self.assessments],
            'attachment': self.attachment,
            'attachmentName': self.attachment_name,
            'trackIcon': self.track_icon
        }


# Association table for the Domain to Scenario many to many relationship
domain_scenario_table = Table('domain_scenario', Base.metadata, Column('domain_id', Integer, ForeignKey('domains.id')),
                              Column('scenario_id', Integer, ForeignKey('scenarios.id')))

# Association table for the Domain to ExternalTraining many to many relationship
domain_external_training_table = Table('domain_external_training', Base.metadata, Column('domain_id', Integer, ForeignKey('domains.id')),
                              Column('external_training_id', Integer, ForeignKey('external_trainings.id')))


class Domain(AbstractBase):
    """ Tracks consist of many scenarios. A user can be assigned a track which
    will grant them access to multiple scenarios.
    """
    __tablename__ = 'domains'
    id = Column(Integer, primary_key=True)
    domain_name = Column(String(256))
    bonus_points = Column(Integer, default=0)
    scenarios = relationship("Scenario",
                             secondary=domain_scenario_table,
                             backref="domains")
    external_trainings = relationship("ExternalTraining",
                                      secondary=domain_external_training_table,
                                      backref="domains")

    @property
    def serialize(self):
        return {
            'id': self.id,
            'domainName': self.domain_name,
            'bonusPoints': self.bonus_points,
            'scenarios': [s.serialize for s in self.scenarios]
        }


class Assessment(AbstractBase):
    __tablename__ = 'assessments'
    id = Column(Integer, primary_key=True)
    name = Column(String(256), index=True)
    level = Column(String(32), index=True)
    points_possible = Column(Integer)
    num_questions = Column(Integer)


    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'level': self.level,
            'pointsPossible': self.points_possible
        }

    def __repr__(self):
        return self.name + " : " + self.level


class AssessmentAssignment(Assignment):
    __tablename__ = 'assessment_assignments'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    assessment_id = Column(Integer, ForeignKey('assessments.id'))
    track_id = Column(Integer, ForeignKey('tracks.id'))
    domain_id = Column(Integer, ForeignKey('domains.id'))
    type = Column(String(256))
    track_name = Column(String(256))
    domain_name = Column(String(256))
    assessment = relationship("Assessment")
    track = relationship("Track")
    domain = relationship("Domain")
    correct = Column(Integer)
    wrong = Column(Integer)

    @property
    def serialize(self):
        if self.track:
            ser_track = self.track.serialize
        else:
            ser_track = ""
        if self.domain:
            ser_domain = self.domain.serialize
        else:
            ser_domain = ""
        if self.completed and self.correct is not None:
            grade = round((float(self.correct) / float(self.assessment.num_questions)) * 100, 2)
            grade = str(grade) + "%"
        else:
            grade = ""
        return {
            'id': self.id,
            'assessment': self.assessment.serialize,
            'domain': ser_domain,
            'track': ser_track,
            'completed': self.completed,
            'completed_at': self.completed_at,
            'grade': grade,
            'correct': self.correct
        }


def track_is_complete(track):
    # Check if all scenario, assessment, and external training assignments are complete
    completed = 0
    # Look at each scenario in the track and check if user has a completed scenario that matches
    for scenario in track.track.scenarios:
        # Check for completed ScenarioAssignments
        for assignment in track.user.scenario_assignments:
            if assignment.scenario == scenario and assignment.completed:
                completed += 1
    for tng in track.track.external_trainings:
        # Check for completed ScenarioAssignments
        for assignment in track.user.external_training_assignments:
            if assignment.external_training == tng and assignment.completed:
                completed += 1
    for assessment in track.track.assessments:
        # Check for completed ScenarioAssignments
        for assignment in track.user.assessment_assignments:
            if assignment.assessment == assessment and assignment.completed:
                completed += 1

    items_to_complete = len(track.track.scenarios+track.track.external_trainings+track.track.assessments)

    return completed + 1 == items_to_complete


@event.listens_for(ScenarioAssignment.completed, 'set', named=True)
def scenario_assignment_set(**kw):
    "listen for the 'set' event"
    target = kw['target']
    value = kw['value']
    if target.track is None:
        return
    track_assignment = TrackAssignment.query.filter_by(user_id=target.user_id, track_id=target.track.id).first()
    if track_is_complete(track_assignment):
        # Find track assignment that matches track
        track_assignment.completed = True
        db_session.add(track_assignment)
        db_session.commit()


@event.listens_for(AssessmentAssignment.completed, 'set', named=True)
def assessment_assignment_set(**kw):
    "listen for the 'set' event"
    target = kw['target']
    value = kw['value']
    if target.track is None:
        return
    track_assignment = TrackAssignment.query.filter_by(user_id=target.user_id, track_id=target.track.id).first()
    if track_is_complete(track_assignment):
        # Find track assignment that matches track
        track_assignment.completed = True
        db_session.add(track_assignment)
        db_session.commit()


@event.listens_for(ExternalTrainingAssignment.completed, 'set', named=True)
def external_training_assignment_set(**kw):
    "listen for the 'set' event"
    target = kw['target']
    value = kw['value']
    if target.track is None:
        return
    track_assignment = TrackAssignment.query.filter_by(user_id=target.user_id, track_id=target.track.id).first()
    if track_is_complete(track_assignment):
        # Find track assignment that matches track
        track_assignment.completed = True
        db_session.add(track_assignment)
        db_session.commit()


@event.listens_for(DomainAssignment.completed, 'set', named=True)
def domain_assignment_set(**kw):
    "listen for the 'set' event"
    target = kw['target']
    value = kw['value']


@event.listens_for(TrackAssignment.completed, 'set', named=True)
def track_assignment_set(**kw):
    "listen for the 'set' event"
    target = kw['target']
    value = kw['value']