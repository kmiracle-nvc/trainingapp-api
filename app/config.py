import sys


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = "A0Zr98j/3yX R~XHH!jmN]LWX/,?RT"
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    #DB_CONN_STRING


class ProductionConfig(Config):
    NAME = "production"
    DEBUG = False


class StagingConfig(Config):
    NAME = "staging"
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    NAME = "development"
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    NAME = "testing"
    TESTING = True
    DEBUG = True