from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from app import app

if app.config["NAME"] == "development":
    engine = create_engine('sqlite:////tmp/test.db', convert_unicode=True)
elif app.config["NAME"] == "testing":
    engine = create_engine('mysql://USER:PASSWORD@localhost/trainingapp', convert_unicode=True, pool_recycle=3600)


db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import app.models
    Base.metadata.create_all(bind=engine)