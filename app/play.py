BASE = 'base'


def valid_scenario_file(scenario_file):
    """
    Determine if the scenario file has all required information and fields.
    :param scenario_file: JSON scenario file
    :return: bool
    """
    pass


def next_question(game):
    # Determine the next question based on the last stage
    brief = ""
    if game.last_stage == BASE:
        brief = game.scenario.json_obj["brief"]
    else:
        brief = find_stage(game.scenario.json_obj["base"], game.last_stage)["brief"]
    return brief


def available_commands(game):
    if game.last_stage == BASE:
        commands = [stage["command"] for stage in game.scenario.json_obj["base"]]
    else:
        commands = [stage["command"] for stage in find_stage(game.scenario.json_obj["base"], game.last_stage)["stages"]]
    return commands


def find_stage(base_stages, stage_name):
    for stage in base_stages:
        if stage["name"] == stage_name:
            return stage
        elif "stages" in stage:
            return find_stage(stage["stages"], stage_name)


def submit_answer(game, answer):
    correct = False
    is_win = False
    message = ""
    findings = []
    points = 0
    last_stage = game.last_stage

    if last_stage == BASE:
        for stage in game.scenario.json_obj["base"]:
            if stage["command"] == answer:
                if "isFail" not in stage:
                    if "successMessage" in stage:
                        message = stage["successMessage"]
                        points = stage["points"]
                    elif "winMessage" in stage and "isWin" in stage:
                        message = stage["winMessage"]
                        points = stage["points"]
                        is_win = True
                    findings = stage["scenarioFindings"]
                    correct = True
                    next_stage = stage["name"]
                    break
                else:
                    message = stage["failMessage"]
                    next_stage = BASE
                    break
    else:
        for stage in find_stage(game.scenario.json_obj["base"], last_stage)["stages"]:
            if stage["command"] == answer:
                if "isFail" not in stage:
                    if "successMessage" in stage:
                        points = stage["points"]
                        message = stage["successMessage"]
                    elif "winMessage" in stage and "isWin" in stage:
                        points = stage["points"]
                        message = stage["winMessage"]
                        is_win = True
                    findings = stage["scenarioFindings"]
                    correct = True
                    next_stage = stage["name"]
                    break
                else:
                    message = stage["failMessage"]
                    next_stage = last_stage
                    break

    return {"correct": correct,
            "message": message,
            "findings": findings,
            "points": points,
            "next_stage": next_stage,
            "is_win": is_win}


def grade_assessment(answers):
    correct = 0
    total = 15
    summary = {}

    if answers.get("one") == "a":
        correct += 1
        summary["one"] = "correct"
    else:
        summary["one"] = "incorrect"

    if answers.get("two") == "b":
        correct += 1
        summary["two"] = "correct"
    else:
        summary["two"] = "incorrect"

    if answers.get("three") == "c":
        correct += 1
        summary["three"] = "correct"
    else:
        summary["three"] = "incorrect"

    if answers.get("four") == "d":
        correct += 1
        summary["four"] = "correct"
    else:
        summary["four"] = "incorrect"

    if answers.get("five") == "a":
        correct += 1
        summary["five"] = "correct"
    else:
        summary["five"] = "incorrect"

    if answers.get("six") == "b":
        correct += 1
        summary["six"] = "correct"
    else:
        summary["six"] = "incorrect"

    if answers.get("seven") == "c":
        correct += 1
        summary["seven"] = "correct"

    if answers.get("eight") == "d":
        correct += 1
        summary["eight"] = "correct"
    else:
        summary["eight"] = "incorrect"

    if answers.get("nine") == "a":
        correct += 1
        summary["nine"] = "correct"
    else:
        summary["nine"] = "incorrect"

    if answers.get("ten") == "b":
        correct += 1
        summary["ten"] = "correct"
    else:
        summary["ten"] = "incorrect"

    if answers.get("eleven") == "b":
        correct += 1
        summary["eleven"] = "correct"
    else:
        summary["eleven"] = "incorrect"

    if answers.get("twelve") == "b":
        correct += 1
        summary["twelve"] = "correct"
    else:
        summary["twelve"] = "incorrect"

    if answers.get("thirteen") == "b":
        correct += 1
        summary["thirteen"] = "correct"
    else:
        summary["thirteen"] = "incorrect"

    if answers.get("fourteen") == "b":
        correct += 1
        summary["fourteen"] = "correct"
    else:
        summary["fourteen"] = "incorrect"

    if answers.get("fifteen") == "b":
        correct += 1
        summary["fifteen"] = "correct"
    else:
        summary["fifteen"] = "incorrect"

    return {
        "grade": (correct, total),
        "summary": summary,
        "correct": correct
    }